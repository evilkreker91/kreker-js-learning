// Назва JavaScript
let question = prompt('Яка "офіційна" назва JavaScript?');
if (question == "ECMAScript") {
  alert("Правильно!");
} else {
  alert("Ви не знаєте? ECMAScript!");
}

// Покажіть знак
let showSign = prompt("Введіть будь-яке числове значення");
if (showSign > 0) {
  alert("1");
} else if (showSign < 0) {
  alert("-1");
} else if (showSign === "0") {
  alert("0");
} else if (showSign === null) {
  alert('Скасовано!');
} else if (showSign === "") {
  alert("Нє, ну тобі шо взападло шоь написати?");
} else {
  alert("Вас попросили ввести числове значення! Хіба це так важко???");
}

// Перепишіть 'if' на '?'
let a = 1;
let b = 2;
let result = a + b < 4 ? "Нижче" : "Вище";
console.log(result);

// Перепишіть 'if..else' на '?'
let statusInput = prompt("Ану напиши мені хто ти там по жизні?");
let message =
  statusInput == "Працівник"
    ? "Привіт салага!"
    : statusInput == "Директор"
    ? "Доброго дня Шановний!"
    : statusInput == ""
    ? "Нема логіну!"
    : "В нас нема такої вакансії!";

alert(message);
