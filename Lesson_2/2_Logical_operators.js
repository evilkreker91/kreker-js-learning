// Який буде результат?
console.log(null || 2 || undefined); // 2
console.log(console.log(1) || 2 || console.log(3)); // 1 потім 2
console.log(1 && null && 2); // null
console.log(console.log(1) && console.log(2)); // 1 потім undefined
console.log(null || (2 && 3) || 4); // 3

// Перевірте діапазон
let ageCheck = prompt("Скільки тобі повних років?");
if (ageCheck >= 14 && ageCheck <= 90) {
  alert("Супер! Нам підходить!");
} else {
  alert("Ви не в нашому діапазоні! Сорян...");
}

// Перевірте значення поза діапазоном - 1
let ageCheckOne = prompt("Скільки тобі повних років? (1)");
if (!(ageCheckOne >= 14 && ageCheck <= 90)) {
  alert("Ви в діапазоні!");
} else {
  alert("Ви не в діапазоні! Сорян...");
}

// Перевірте значення поза діапазоном - 2
let ageCheckTwo = prompt("Скільки тобі повних років? (2)");
if (ageCheckTwo < 14 || ageCheck > 90) {
  alert("Ви в діапазоні!");
} else {
  alert("Ви не в діапазоні! Сорян...");
}

// Які з цих alert буде виконано?
if (-1 || 0) alert("перший"); // Працює
if (-1 && 0) alert("другий"); // Не працює
if (null || (-1 && 1)) alert("третій"); // Працює

// Перевірте логін
let authName = prompt("Enter Your Login please");
if (authName === "Admin") {
  let pass = prompt("Enter Your password please");
  if (pass === "Master") {
    alert("Welcome Master!");
  } else if (pass === "" || pass === null) {
    alert("Canceled!");
  } else {
    alert("Wrong password!");
  }
} else if (authName === "" || authName === null) {
  alert("Canceled!");
} else {
  alert("I Don't know You!");
}
